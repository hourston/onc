# Ocean Networks Canada Data Summary

__Main author:__    Cole Fields
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel:  

- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)

## Objective
The main goal of this project is to use the Ocean Networks Canada (ONC) API to query their metadata and populate a spreadsheet with device deployments.

## Summary
This project aims to automate the retrieval of device deployment data from ONC using their API. The inputs for this project are the ONC API and the desired parameters for querying metadata. The output is a spreadsheet containing information about device deployments, including deployment dates, locations, and device details. To create a csv file, run `python onc.py --csv`. A `.env` file that contains your ONC API Token must exist in the same directory. Do not expose the API key. The `.env` text file should be formatted such as ONC_TOKEN=11111-2222-3333-44 and your token can be obtained from the Web Services API tab of https://data.oceannetworks.ca/Profile.

## Status
In-development

## Contents
The contents of this repository include:
- `onc_functions.py`: Python script for querying ONC metadata and creating a CSV of device metadata.
- `onc.py`: Python script for using various functions from the Oceans 3.0 Public API.
- `settings.py`: Python file primarily responsible for loading environment variables and setting global variables based on the content of the `.env` file. 

## Methods
The `onc.py` script serves as the entry point for interacting with the ONC API. It uses functions from `onc_functions.py` to perform the following tasks based on command-line arguments:
- Creating a CSV file listing devices and deployment information.
- Printing a list of device category codes from ONC.
- Printing a list of data products by specifying a device category code.
- Printing region and its subregions with location codes by specifying a region.
- Downloading data based on specified category and extension.

## Requirements
- Python 3.x
- Requests library for sending HTTP requests to the ONC API
- JSON library for parsing API responses
- CSV library for generating the spreadsheet
- Pandas library for handling data frames
- python-dotenv library for loading environment variables from a `.env` file

## Caveats
Users should be aware of the following caveats when using the `onc.py` script:
- The accuracy of the output depends on the data available through the ONC API.
- Ensure appropriate usage of the ONC API according to their terms of service and data usage policies.
- Column names in the output csv file are not exactly matching the calls made to the API.

## Uncertainty
There may be uncertainty associated with the output, especially if the ONC API data changes or if there are gaps in the data.

## Acknowledgements
Acknowledgments to ONC for providing access to their API and data.

## References
*Optional section.*
- [ONC API Documentation](https://data.oceannetworks.ca/OpenAPI)
- [ONC Data Usage Policy](https://www.oceannetworks.ca/data/data-policy/)
