"""
Load environment from .env file and set global variables.
"""

from dotenv import load_dotenv
import os
from datetime import datetime

load_dotenv()

TODAY = datetime.today().strftime('%Y%m%d')
API_URL = 'https://data.oceannetworks.ca/api/'
METHOD = 'get'
ONC_TOKEN = os.environ.get('ONC_TOKEN')
CSV_PATH = os.path.join('output', f'onc_data_{TODAY}.csv')
