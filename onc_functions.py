"""
Functions for printing and creating a csv of device metadata from 
Ocean Networks Canada Oceans 3.0 data portal https://data.oceannetworks.ca/home.
"""

import settings
import requests
import pandas as pd
import os
import logging

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


def list_categories():
    """ Print device categories. """
    request_string = f'{settings.API_URL}deviceCategories?method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    response_json = response.json()
    codes, names = [], []
    for device in response_json:
        codes.append(device['deviceCategoryCode'])
        names.append(device['deviceCategoryName'])
    max_code_width = len(max(codes, key=len))
    header = 'CATEGORY CODE'
    header_output = f'{header.ljust(max_code_width)} CATEGORY NAME'
    print('-' * len(header_output))
    print(header_output)
    print('-' * len(header_output))
    for c, n  in zip(codes, names):
        c_string = c.ljust(max_code_width)
        output = f'{c_string} {n}'
        print(output)


def print_region(region):
    """ Print region, location codes, and names. """
    region_string = f"{region['locationName']} ({region['locationCode']})"
    print('*' * len(region_string))
    print(region_string)
    print('*' * len(region_string))
    subregions = region['children']
    codes, names, children = [], [], []
    for sub in subregions:
        codes.append(sub['locationCode'])
        names.append(sub['locationName'])
        children.append(sub['children'])
    for code, name, child in zip(codes, names, children):
        header = f'{name} ({code})'
        print('-' * len(header))
        print(header)
        print('-' * len(header))
        for c in child:
            child_code = c['locationCode']
            child_name = c['locationName']
            print(f'    {child_name} ({child_code})')
            request_string = f'{settings.API_URL}deviceCategories?locationCode={child_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
            response = requests.get(request_string)
            response_json = response.json()
            for device in response_json:
                print(f"     - {device['deviceCategoryName']} {device['deviceCategoryCode']}")


def list_products(code):
    """ Print data products available by specifying a code. """
    request_string = f'{settings.API_URL}dataProducts?deviceCategoryCode={code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    response_json = response.json()
    codes, names, extensions = [], [], []
    for device in response_json:
        codes.append(device['dataProductCode'])
        names.append(device['dataProductName'])
        extensions.append(device['extension'])
    max_code_width = len(max(codes, key=len))
    max_name_width = len(max(names, key=len))
    hstring, nstring = 'CODE', 'NAME'
    header_output = f'{hstring.ljust(max_code_width)} {nstring.ljust(max_name_width)} EXTENSION'
    print('-' * len(header_output))
    print(header_output)
    print('-' * len(header_output))
    for c, n, e  in zip(codes, names, extensions):
        c_string = c.ljust(max_code_width)
        output = f'{c_string} {n.ljust(max_name_width)} {e}'
        print(output)


def print_location_info(location_dict, indent=0):
    if location_dict is None:
        return
    location_name = location_dict.get('locationName')
    location_code = location_dict.get('locationCode')
    if location_name is not None and location_code is not None:
        print(f"{' ' * indent}Location: {location_name} (Code: {location_code})")
    children = location_dict.get('children')
    if children is not None:
        for child in children:
            print_location_info(child, indent + 2)


def list_locations(region):
    print_region(region)


def get_device_categories(location_code=None):
    """ Return device category  data dictionaries (optionally) by LOCATION_CODE. """
    logging.info('Getting device categories.')
    if location_code:
        request_string = f'{settings.API_URL}deviceCategories?locationCode={location_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    else:
        request_string = f'{settings.API_URL}deviceCategories?method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    if response.status_code == 200:
        return response.json()
    return


def get_deployments(location_code, category_code=None):
    """ Return deployments data dictionaries filtered by LOCATION_CODE and (optionally) device CATEGORY_CODE. """
    if category_code:
        logging.info(f'Getting deployments at {location_code}.')
        request_string = f'{settings.API_URL}deployments?locationCode={location_code}&deviceCategoryCode={category_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    else:
        logging.info(f'Getting deployment metadata: {location_code} [{category_code}].')
        request_string = f'{settings.API_URL}deployments?locationCode={location_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    if response.status_code == 200:
        return response.json()
    return


def get_data_products(location_code, category_code=None):
    """ Return data products dictionaries filtered by LOCATION_CODE and (optionally) CATEGORY_CODE. """
    if category_code:
        logging.info(f'Getting data products at {location_code}.')
        request_string = f'{settings.API_URL}dataProducts?locationCode={location_code}&deviceCategoryCode={category_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    else:
        logging.info(f'Getting data product metadata: {location_code} [{category_code}].')
        request_string = f'{settings.API_URL}dataProducts?locationCode={location_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    if response.status_code == 200:
        return response.json()
    return


def get_locations(location_code, category_code=None):
    """ Return data dictionaries for locations filtered by LOCATION_CODE. """
    if category_code:
        logging.info(f'Getting location metadata at {location_code}.')
        request_string = f'{settings.API_URL}locations?locationCode={location_code}&deviceCategoryCode={category_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    else:
        logging.info(f'Getting location metadata: {location_code}:  [{category_code}].')
        request_string = f'{settings.API_URL}locations?locationCode={location_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    if response.status_code == 200:
        return response.json()[0]
    return


def get_device(location_code, device_code):
    """ Return device metadata dictionary by LOCATION_CODE and DEVICE_CODE. """
    logging.info(f'Getting location metadata: {location_code} [{device_code}].')
    request_string = f'{settings.API_URL}devices?deviceCode={device_code}&locationCode={location_code}&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    if response.status_code == 200:
        return response.json()[0]
    return


def flatten_location_info(location):
    """ Return list of dictionaries from location tree nested dictionary. 
         Recurse until node has no children. At this point, the node should have device data to retrieve.
    """
    logging.info('Flattening nested location dictionary.')
    result = []
    def flatten_recursive(node, parent_name=""):
        nonlocal result
        location_name = node.get('locationName', '')
        full_location_name = f"{parent_name} - {location_name}" if parent_name else location_name
        if 'children' in node and node['children']:
            for child in node['children']:
                flatten_recursive(child, full_location_name)
        else:
            result.append({
                'locationName': full_location_name,
                'description': node.get('description', ''),
                'hasDeviceData': node.get('hasDeviceData', ''),
                'locationCode': node.get('locationCode', ''),
                'hasPropertyData': node.get('hasPropertyData', ''),
            })
    flatten_recursive(location)
    return result


def get_region(tree, region):
    """ Return  entry from TREE for one of REGIONS: ['Arctic', 'Atlantic', 'Mobile Platforms', 'Pacific']. """
    for r in tree[0]['children']:
        if r['locationName'] == region:
            return r


def get_location_tree():
    """ Return location tree (hierarchical). """
    logging.info('Getting location tree dictionary.')
    request_string = f'{settings.API_URL}locations-tree?&method={settings.METHOD}&token={settings.ONC_TOKEN}'
    response = requests.get(request_string)
    if response.status_code == 200:
        return response.json()
    return


def all_not_none(*items):
    """ Return whether all items are not None. """
    return all(item is not None for item in items)


def make_dataframe(data_dict_list):
    """ Return a Pandas DataFrame with device information from DATA_DICT_LIST. """
    data = []
    for entry in data_dict_list:
        location_name = entry['locationName']
        location_code = entry['locationCode']
        categories = get_device_categories(location_code)
        # categories may be None if response code != 200 (e.g., 404 where no results found.)
        if categories:
            for category in categories:
                device_name = category.get('deviceCategoryName')
                device_category_code = category.get('deviceCategoryCode')
                device_category_name = category.get('deviceCategoryName')
                logging.info(f'Retrieving metadata: {location_name}: [{device_category_name}].')
                locations = get_locations(location_code, device_category_code)
                deployments = get_deployments(location_code, device_category_code)
                products = get_data_products(location_code, device_category_code)
                if not all_not_none(locations, deployments, products):
                    continue
                product_names = [p.get('dataProductName') for p in products]
                product_extensions = [p.get('extension') for p in products]
                for deployment in deployments:
                    device_code = deployment.get('deviceCode')
                    device = get_device(location_code, device_code)
                    data.append({
                        'LocationFullName': location_name,
                        'LocationCode': location_code,
                        'DeviceCategoryName': device_category_name,
                        'DeviceCode': device_category_code,
                        'DeviceName': device_name,
                        'Latitude': locations.get('lat'),
                        'Longitude': locations.get('lon'),
                        'LocationDescription': locations.get('description'),
                        'DateStart': deployment.get('begin'),
                        'DateEnd': deployment.get('end'),
                        'Depth': deployment.get('depth'),
                        'ProductNames': product_names,
                        'ProductExtensions': product_extensions,
                        'DeviceName': device.get('deviceName'),
                        'DeviceLink': device.get('deviceLink')
                    })
    return pd.DataFrame(data)


def save_dataframe(df):
    """ Write dataframe to disk in csv format. """
    cwd = os.getcwd()
    csv_path = os.path.join(cwd, settings.CSV_PATH)
    if not os.path.exists(os.path.dirname(csv_path)):
        os.mkdir(os.path.dirname(csv_path))
    logging.info(f'Saving DataFrame as csv at: {csv_path}.')
    df.to_csv(csv_path, index=False)
    return


def download_product(category, extension):
    """ Download data product. """
    return



def process(inargs):
    make_csv = inargs.csv
    print_categories = inargs.pc
    print_products = inargs.pp
    region = inargs.pr
    download = inargs.download
    category = inargs.category
    extension = inargs.extension
    if make_csv:
        tree = get_location_tree()
        pacific = get_region(tree, 'Pacific')
        flat = flatten_location_info(pacific)
        df = make_dataframe(flat)
        save_dataframe(df)
        return
    if print_categories:
        list_categories()
    if print_products:
        list_products(category)
    if print_region:
        location_tree = get_location_tree()
        region_dict = get_region(location_tree, region)
        list_locations(region_dict)
    if download:
        download_product(category, extension)
