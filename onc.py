""" 
This program uses various functions from the Oceans 3.0 Public API.
https://data.oceannetworks.ca/OpenAPI#overview

It can print device categories and data products. A user can also download data
by specifying a format (extension) and data product code.
"""

import onc_functions
import settings
import os
import sys

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', action='store_true', help='Create csv file listing devices and deployment information.')
    parser.add_argument('--pc', action='store_true', help='Print list of device category codes from ONC.')
    parser.add_argument('--pp', action='store_true', help='Print list of data products by specifying device category code.')
    parser.add_argument('--pr', choices=['Arctic', 'Atlantic', 'Pacific'], help='Print region and its subregions with location codes by specifying a region.')
    parser.add_argument('-d', '--download', action='store_true', help='Print list of device category codes from ONC.')
    # Add the required string argument (only if the download flag is provided)
    parser.add_argument('-c', '--category', required='--download' in sys.argv, help='Required string argument when --download is provided')
    parser.add_argument('-e', '--extension', required='--download' in sys.argv, help='Required string argument when --download is provided')
    args = parser.parse_args()
    if args.pp and not args.category:
        parser.error('--pp requires --category to be provided')
    if args.download and not (args.category and args.extension):
        parser.error('--download requires -c (--category) and -e (--extension) to be provided')
    onc_functions.process(args)


if __name__ == '__main__':
    main()
